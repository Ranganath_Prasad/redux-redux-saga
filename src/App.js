import {Container} from 'semantic-ui-react';
import './App.css';
import MainHeader from './components/MainHeader';
import NewEntryForm from './components/NewEntryForm';
import DisplayBalance from './components/DisplayBalance';
import DisplayBalances from './components/DisplayBalances';
import EntryLines from './components/EntryLines';
import { useEffect, useState } from 'react';
import ModalEdit from './components/ModalEdit';
import {useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
import { getAllEntries } from './actions/entries.actions';

function App() {

  const [incomeTotal, setIncomeTotal] = useState(0);
  const [expenseTotal, setExpenseTotal] = useState(0);
  const [total, setTotal] = useState(0);
  const [entry, setEntry] = useState(null);

  const entries = useSelector((state) => state.entries);
  const {isOpen, id} = useSelector( (state) => state.modals);

  useEffect( () =>{
    const index = entries.findIndex( entry => entry.id === id);
    setEntry(entries[index])
  },[isOpen, id, entries])

  useEffect( () => {
    let totalIncome = 0;
    let totalExpense = 0;
    entries.map(entry => {
      if(entry.isExpense){
        return totalExpense += Number(entry.value)
      }else{
        return totalIncome += Number(entry.value)
      }
    })
    let total = totalIncome - totalExpense;
    setTotal(total);
    setExpenseTotal(totalExpense);
    setIncomeTotal(totalIncome);
  },[entries])
  
  
  const dispatch = useDispatch();
  useEffect( () =>{
    dispatch(getAllEntries());
  },[])

  return (
    <Container>
      <MainHeader title='Budget'/>
      <DisplayBalance title='Your Balanace:' value={total} size ='small'/>
      <DisplayBalances incomeTotal={incomeTotal} expenseTotal={expenseTotal}/>
      <MainHeader title='History' type='h3'/>
      <EntryLines entries={entries}/>
      <MainHeader title='Add a new transaction' type='h3'/>
      <NewEntryForm/>
      <ModalEdit isOpen={isOpen} {...entry}/>
    </Container>
  );
}

export default App;