import React from 'react';
import {Container} from 'semantic-ui-react';
import EntryLine from './EntryLine';

function EntryLines({
    entries 
}){
    return (
        <Container>
            {
            entries.map( (record) => (
                <EntryLine
                    key={record.id}
                    {...record}
                />
            ))
        }
        </Container>  
    )
}

export default EntryLines;